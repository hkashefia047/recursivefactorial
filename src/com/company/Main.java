package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Scanner in=new Scanner(System.in);
        System.out.println(RecursiveFact(in.nextInt()));

    }
    public static int RecursiveFact(int n){
        if (n==0){
            return 1;
        }
        return n*RecursiveFact(n-1);
    }

}
